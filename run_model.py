import torch
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
from torch import Tensor
import math
from PIL import Image
from BEITModel import BEITModel
from itertools import chain
import utils as utils
from torchvision.utils import save_image
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-i", "--image_path", help="path to the input image")
parser.add_argument("-x", "--x_idxs", help="x indices of masked patches")
parser.add_argument("-y", "--y_idxs", help="y indices of masked patches")

args = parser.parse_args()


# TODO: read in image and indices from command line
image_path = args.image_path
print(image_path)

x_idxs = utils.idxs_string_to_list(args.x_idxs)
y_idxs = utils.idxs_string_to_list(args.y_idxs)
print(x_idxs)
print(y_idxs)

# load model
PATH = './BEIT_model3.pth'
network = BEITModel()
network.load_state_dict(torch.load(PATH))

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

img = Image.open(image_path)
img = transform(img)
# mask desired patches

utils.mask_image_at_idxs(img, x_idxs, y_idxs)  # inplace
img = img.unsqueeze(0)


flat_patches = utils.image_to_flat_patches(img)
trained_patches = network(flat_patches)
trained_patches = trained_patches.squeeze().unsqueeze(0)
trained_patches = utils.flat_patches_to_patches(trained_patches, 4)
trained_image = utils.patches_to_images(trained_patches)

save_image(trained_image.squeeze()/ 2 + 0.5, image_path[:-4] + "_trained" + image_path[-4:])
