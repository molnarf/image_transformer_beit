# syntax=docker/dockerfile:1
FROM python:3.8.7
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN pip install -r requirements.txt
CMD ["python", "application.py"]
