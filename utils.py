import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
from torch import Tensor
import math
from PIL import Image
from BEITModel import BEITModel
from itertools import chain
import utils as utils


patch_width = 4 # patch_width = patch_height
step = 4
n_channels = 3
num_patches = int(32**2 / patch_width**2)
flat_patch_size = patch_width**2 * 3


def idxs_string_to_list(idxs):
	if not idxs:
		return list()
	
	return list(map(int, idxs.split(",")))

def imshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.detach().numpy()
    fig, ax = plt.subplots(figsize=(8, 8))
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()


def image_to_flat_patches(image_batch):
    """
    Partition the image into square patches and flatten them. 
    Color channels are concatenated.
    
    Args: 
        image_batch: shape [batch_size, n_channels, width, height]
    
    Return:
        flat_patches: shape [batch_size, num_patches, patch_length^2 * n_channels]
    """
    # Divide the image into patches

    patches = image_batch.unfold(2, patch_width, step).unfold(3, patch_width, step).permute(0, 2, 3, 1, 4, 5).reshape(image_batch.shape[0], -1, n_channels, patch_width, patch_width)
    flat_patches = patches.reshape(image_batch.shape[0], num_patches, -1) # shape: [batch_size, n_patches, patch_length^2 * n_channels]

    return flat_patches
    

def flat_patches_to_patches(flat_patches, patch_width):
    patches = flat_patches.reshape(flat_patches.shape[0], num_patches, 3, patch_width, patch_width) # reshape back to 2D patches
    return patches
    
def patches_to_images(patches):
    # patches shape: [batch_size, n_patches, n_channels, patch_width, patch_width]
    # output shape: [batch_size, n_channels, 32, 32]
    fold = nn.Fold(output_size=(32, 32), kernel_size=(4, 4), stride=(4, 4))

    patches = patches.reshape(patches.shape[0], patches.shape[1], -1)
    patches = patches.permute(0, 2, 1)
    patches = fold(patches)
    #patches = patches.permute(0, 2, 1, 3, 4)
    return patches.reshape(patches.shape[0], 3, 32, 32)
    
	


# one patch has width/height 4*4
def patch_idx_to_positions(x_idxs, y_idxs):
	x_positions, y_positions = [], [] # initialize in case x_idxs and y_idxs are empty
	for x_idx, y_idx in zip(x_idxs, y_idxs):
		# x_positions for idx 0: [0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3]
		# y_positions for idx 0:
		x_positions = [list(range(idx*4, idx*4+4))*4 for idx in x_idxs]  
		x_positions = list(chain.from_iterable(x_positions))  # unlist
		y_positions = [range(idx*4, idx*4+4) for idx in y_idxs]
		y_positions = list(np.repeat(y_positions, 4))
		
	return x_positions, y_positions
    

# image shape: [3, 32, 32]
# x_idxs: list of x idx to be masked  (0 <= idx <= 7)
# y_idxs: list of y ixs to be masked
def mask_image_at_idxs(image, x_idxs, y_idxs):
    x_positions, y_positions = patch_idx_to_positions(x_idxs, y_idxs)
    image[:, y_positions, x_positions] = 0
    

