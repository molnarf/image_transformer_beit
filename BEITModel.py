import torch
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
from torch import Tensor
import math
from PIL import Image


class BEITModel(nn.Module):
    # patch_size = patch_width * patch_height * 3
    def __init__(self, patch_size: int = 48, nhead: int = 3, d_hid: int = 128, n_layers: int = 3):
        super().__init__()
        self.encoder_layer = nn.TransformerEncoderLayer(d_model=patch_size, nhead=nhead, dim_feedforward=d_hid,
                                                    dropout=0, batch_first=True)
        self.transformer_encoder = nn.TransformerEncoder(self.encoder_layer, num_layers=n_layers)
        self.pos_encoder = PositionalEncoding(patch_size)
        self.last_layer = nn.Linear(patch_size, patch_size)
        self.tanh = nn.Tanh()

    def forward(self, masked_patches) -> Tensor:
        """
        Args:
            flat_patches: Tensor of shape: [batch_size, n_patches, patch_size] 
            (n_patches = seq_len, patch_size = patch_length^2 * n_channels).
        Returns:
            predicted_patches: Tensor of same shape as patches.
        """
        
        masked_patches_pos = self.pos_encoder(masked_patches)
        #masked_idxs is a list of masked indices (each entry is a tensor of shape: [])
        trained_patches = self.transformer_encoder(masked_patches_pos)
        trained_patches = self.last_layer(trained_patches)
        trained_patches = self.tanh(trained_patches)
        
        return trained_patches
        
		
		
class PositionalEncoding(nn.Module):

    def __init__(self, d_model: int, dropout: float = 0.0, max_len: int = 2000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        position = torch.arange(max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2) * (-math.log(10000.0) / d_model))
        pe = torch.zeros(max_len, d_model)
        pe[:, 0::2] = torch.sin(position * div_term)
        # without this modification, it would throw an error for uneven d_model
        if d_model % 2 == 0:
            pe[:, 1::2] = torch.cos(position * div_term)
        else:
            pe[:, 1::2] = torch.cos(position * div_term)[:, 0:-1]
        pe = pe.unsqueeze(0)  # pe shape[1, max_len, d_model]
        self.register_buffer('pe', pe)

    def forward(self, x: Tensor) -> Tensor:
        """
        Args:
            x: Tensor of embedded peaks, shape [batch_size, n_patches, patch_size]  (patch_size is equal to d_model)
        """
        x = x + self.pe[:, :x.size(1)]
        x = x.squeeze(0)
        return self.dropout(x)
		
		

