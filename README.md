# Image Transformer - BEIT 
(Bidirectional Encoder representations from Image Transformers)

## Trained an image transformer on the cifar10 dataset (self-supervised)

Random patches of the image were masked with the objective to predict the pixel values of the masked regions.

Try it out on the webpage:

    1. "git clone git@gitlab.com:molnarf/image_transformer_beit.git"

    2. Open the termial in the cloned folder and execute "python application.py"

    3. Open "http://127.0.0.1:5000/" on your browser and play around!

Alternatively, run the application via docker:

    "docker-compose up"

<img src="images/screenshot.JPG" width="700">
