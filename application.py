import torch
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
from torch import Tensor
import math
from PIL import Image
from BEITModel import BEITModel
from itertools import chain
import utils as utils
from torchvision.utils import save_image
from argparse import ArgumentParser
from flask import Flask, render_template
import json
import os

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config["CACHE_TYPE"] = "null"

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

@app.route("/")
def index():
	return render_template("index.html")
	

@app.route("/process_image/<string:imageInfo>", methods=["POST"])
def process_image(imageInfo):

	info = json.loads(imageInfo)
	
	filename = info['imageInfo']['filename']
	counter = info['imageInfo']['counter']
	x_idxs = info['imageInfo']['x_idxs']
	y_idxs = info['imageInfo']['y_idxs']
	x_idxs = utils.idxs_string_to_list(x_idxs)
	y_idxs = utils.idxs_string_to_list(y_idxs)
	print("Received Info")
	print(info)


	filename = info['imageInfo']['filename']
	image_path = f"static/{filename}"

	print("Received Info")
	print(info)
	
	# load model
	PATH = './BEIT_model3.pth'
	network = BEITModel()
	network.load_state_dict(torch.load(PATH))

	transform = transforms.Compose(
		[transforms.ToTensor(),
		 transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

	img = Image.open(image_path)
	img = transform(img)
	# mask desired patches

	utils.mask_image_at_idxs(img, x_idxs, y_idxs)  # inplace
	img = img.unsqueeze(0)


	flat_patches = utils.image_to_flat_patches(img)
	trained_patches = network(flat_patches)
	trained_patches = trained_patches.squeeze().unsqueeze(0)
	trained_patches = utils.flat_patches_to_patches(trained_patches, 4)
	trained_image = utils.patches_to_images(trained_patches)

	save_image(trained_image.squeeze()/ 2 + 0.5, image_path[:-4] + "_trained" + counter + image_path[-4:])
	print("Saved Trained Image")

	# remove old image
	if os.path.isfile(image_path[:-4] + "_trained" + str(int(counter)-1) + image_path[-4:]):
		os.remove(image_path[:-4] + "_trained" + str(int(counter)-1) + image_path[-4:])

	return "Info Received"

if __name__ == "__main__":
	app.run(host="0.0.0.0", port=5000, debug=True)
